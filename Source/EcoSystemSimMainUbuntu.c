#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <dirent.h>
#include <omp.h>
//#define NTHREADS 1

#pragma region Structures
typedef struct Cell
{
    int posX, posY;
    char state;
    char directions;
    short age;
    short starve;

} cell;

#pragma endregion

#pragma region Fuction Signatures
cell *FileScan(int *rabbitgenVal, int *foxesGenVal, int *foxesFoodVal, int *Ngen, int *rows, int *cols, int *n, int *countRabbits, int *countFoxes);
cell *BoardFix(cell *cells, int rows, int colum, int n);
void BoardPrint(cell *cells, int rows, int colum);
cell *SetCells(cell *cells, char *string, int *x, int *y, int *countRabbits, int *countFoxes, int j, int *rows, int *cols);
void ClearScreen();
char SetDirection(char *directions, int direction);
int CheckDirection(char *directions, int direction);
char SetBoundaries(char *direction, int x, int y, int row, int col);
void UpdateAdjCells(cell *cells, int n);
int CriteriaAdjCell(int currentGen, cell *cell);
void ChangePos(cell *pos, int dir);
cell *MoveAnimal(cell *animals[], int *animalCount, int currentGen, int procGen, int foodGen, int *n, int row, int col, cell *rabbits[], int *countRabbits, cell *cells, int *newcell);
int CheckRabbits(cell *foxPos, cell *rabbits[], int countRabbits);
void SortAnimals(cell *animals, int *animalCount, int col);
int removeDuplicates(cell *animals, int *animalCount, int col, int *countRabbits, int *countFoxes, int const maxSize);
void UpdateAnimals(cell *cells, int n, cell *rabbits[], int *rabbitsCount, cell *foxes[], int *foxesCount);
#pragma endregion

#pragma region Main Function

int main(int argc, char **argv)
{
    int GEN_PROC_RABBITS = 0;
    int GEN_PROC_FOXES = 0;
    int GEN_FOOD_FOXES = 0;
    int N_GEN = 0;
    int R = 0, C = 0;
    int N = 0;
    int rabbitsCount = 0;
    int foxesCount = 0;

    cell *ptrcells = FileScan(&GEN_PROC_RABBITS, &GEN_PROC_FOXES, &GEN_FOOD_FOXES, &N_GEN, &R, &C, &N, &rabbitsCount, &foxesCount);

    const int maxSize = R * C;

    cell *board = BoardFix(ptrcells, R, C, N);
    printf("Generation n -> %d\n", 0);
    BoardPrint(board, R, C);

    cell *addedCells;
    cell *rabbits[maxSize];
    cell *foxes[maxSize];

    //omp_set_num_threads(NTHREADS);
    for (int i = 0; i < N_GEN; i++)
    {

        //ClearScreen();

        UpdateAnimals(ptrcells, N, rabbits, &rabbitsCount, foxes, &foxesCount);
        //#pragma omp barrier
        printf("Generation n -> %d\n", (i + 1));

        int newcells = 0;
        addedCells = MoveAnimal(rabbits, &rabbitsCount, i, GEN_PROC_RABBITS, GEN_FOOD_FOXES, &N, R, C, rabbits, &rabbitsCount, ptrcells, &newcells);

        if (newcells > 0)
        {
            for (int i = N, j = 0; i < newcells + N && j < newcells; i++)
            {
                //printf("state -> %c posX -> %d posY -> %d age -> %d starve %d\n", addedCells[j].state, addedCells[j].posX, addedCells[j].posY, addedCells[j].age, addedCells[j].starve);
                ptrcells[i] = addedCells[j++];
            }
            N += newcells;
            UpdateAnimals(ptrcells, N, rabbits, &rabbitsCount, foxes, &foxesCount);
        }

        newcells = 0;

        UpdateAdjCells(ptrcells, N);
        // for (int l = 0; l < N; l++)
        // {
        //     printf(" pre cell -> %c in PosX -> %d and PosY -> %d as age -> %d and starve -> %d with directions -> %d\n", ptrcells[l].state, ptrcells[l].posX, ptrcells[l].posY, ptrcells[l].age, ptrcells[l].starve, ptrcells[l].directions);
        // }
        addedCells = MoveAnimal(foxes, &foxesCount, i, GEN_PROC_FOXES, GEN_FOOD_FOXES, &N, R, C, rabbits, &rabbitsCount, ptrcells, &newcells);

        // for (int l = 0; l < N; l++)
        // {
        //     printf(" post cell -> %c in PosX -> %d and PosY -> %d as age -> %d and starve -> %d with directions -> %d\n", ptrcells[l].state, ptrcells[l].posX, ptrcells[l].posY, ptrcells[l].age, ptrcells[l].starve, ptrcells[l].directions);
        // }
        if (newcells > 0)
        {
            for (int i = N, j = 0; i < newcells + N && j < newcells; i++)
            {
                ptrcells[i] = addedCells[j++];
            }
            N += newcells;
        }

        UpdateAdjCells(ptrcells, N);

        removeDuplicates(ptrcells, &N, C, &rabbitsCount, &foxesCount, maxSize);

        if (i <= N_GEN - 1)
        {
            board = BoardFix(ptrcells, R, C, N);
            BoardPrint(board, R, C);
        }
        //free(&addedCells);
        //sleep(1);
    }
    printf("Time passed -> %lf\n", omp_get_wtime());
    return 0;
}

#pragma endregion

#pragma region Functions
char SetDirection(char *directions, int direction)
{
    return *directions ^= (1U << (direction - 1));
}

int CheckDirection(char *directions, int direction)
{
    int ligado = 1;

    return (ligado & (*directions >> direction - 1));
}

char SetBoundaries(char *direction, int x, int y, int row, int col)
{
    if (x == 0 && CheckDirection(direction, 1) == 1)
    {
        *direction = SetDirection(direction, 1);
    }
    else if (x == (col - 1) && CheckDirection(direction, 3) == 1)
    {
        *direction = SetDirection(direction, 3);
    }
    else if (x > 0 && x < (col - 1) && (CheckDirection(direction, 3) == 0 || CheckDirection(direction, 1) == 0))
    {
        *direction = SetDirection(direction, 3);
        *direction = SetDirection(direction, 1);
    }

    if (y == 0 && CheckDirection(direction, 4) == 1)
    {
        *direction = SetDirection(direction, 4);
    }
    else if (y == (row - 1) && CheckDirection(direction, 2) == 1)
    {
        *direction = SetDirection(direction, 2);
    }
    else if (y > 0 && y < (row - 1) && (CheckDirection(direction, 2) == 0 || CheckDirection(direction, 4) == 0))
    {
        *direction = SetDirection(direction, 2);
        *direction = SetDirection(direction, 4);
    }
    return *direction;
}

cell *FileScan(int *rabbitgenVal, int *foxesGenVal, int *foxesFoodVal, int *Ngen, int *rows, int *cols, int *n, int *countRabbits, int *countFoxes)
{

    // char file_name[50];
    // char *direct = ".\\ecosystemsim\\Input\\";
    // FILE *fp = NULL;
    // char *fname = (char *)malloc(10 * (sizeof(char)));
    // printf("Enter name of a file you wish to see\n");
    // if (scanf("%123s", fname))
    // {
    //     file_name[0] = '\0';
    //     strcat(file_name, direct);
    //     strcat(file_name, fname);
    //     free(&fname);
    //     fp = fopen(file_name, "r");
    //     if (fp == NULL)
    //     {
    //         perror("Error while opening the file.\n");
    //         //exit(EXIT_FAILURE);
    //     }
    // }
    int *ptrArr[7] = {rabbitgenVal, foxesGenVal, foxesFoodVal, Ngen, rows, cols, n};

    for (int i = 0; i < 7; i++)
    {
        // if (fp != NULL && fscanf(fp, "%d", ptrArr[i]))
        //     printf("reading File\n");
        // else
        // {
        if (!scanf("%d", ptrArr[i]))
        {
            fprintf(stderr, "Error while reading input.\nAborting...\n");
            exit(1);
        }
        // }
    }
    cell *cells = (cell *)malloc((*rows * *cols) * (sizeof(cell)));
    char *string = (char *)malloc(10 * (sizeof(char)));
    int x = -1, y = -1;
    for (int j = 0; j < *n; j++)
    {
        // if (fp != NULL &&
        //     fscanf(fp, "%s", string) &&
        //     fscanf(fp, "%d", &x) &&
        //     fscanf(fp, "%d", &y))
        // {
        //     SetCells(cells, string, &x, &y, countRabbits, countFoxes, j, rows, cols);
        // }
        //else
        if (scanf("%s", string) &&
            scanf("%d", &x) &&
            scanf("%d", &y))
        {
            SetCells(cells, string, &x, &y, countRabbits, countFoxes, j, rows, cols);
        }
    }

    UpdateAdjCells(cells, *n);
    //fclose(fp);

    return cells;
}

cell *SetCells(cell *cells, char *string, int *x, int *y, int *countRabbits, int *countFoxes, int j, int *rows, int *cols)
{
    if (strcmp("ROCK", string) == 0)
    {
        cells[j].state = '*';
    }
    else if (strcmp("RABBIT", string) == 0)
    {
        cells[j].state = 'R';
        *countRabbits += 1;
    }
    else if (strcmp("FOX", string) == 0)
    {
        cells[j].state = 'F';
        *countFoxes += 1;
    }
    cells[j].posX = *x;
    cells[j].posY = *y;
    cells[j].directions = 15;
    cells[j].age = 0;
    cells[j].starve = 0;

    SetBoundaries(&cells[j].directions, *x, *y, *rows, *cols);
}

void ChangePos(cell *pos, int dir)
{
    switch (dir)
    {
    case 1:
        pos->posX -= 1;
        break;
    case 2:
        pos->posY += 1;
        break;
    case 3:
        pos->posX += 1;
        break;
    case 4:
        pos->posY -= 1;
        break;

    default:
        break;
    }
}
cell *MoveAnimal(cell *animals[], int *animalCount, int currentGen, int procGen, int foodGen, int *n, int row, int col, cell *rabbits[], int *countRabbits, cell *cells, int *newcell)
{
    int moveDir = 0, hasRabbits = 0, isFox = 0, deadFoxes = 0, addedCells = 0, i = 0;
    cell *addedAnimals = (cell *)malloc(*animalCount * (sizeof(cell)));
    cell *newAnimal = (cell *)malloc(sizeof(cell));

    omp_lock_t lock;
    omp_init_lock(&lock);
#pragma omp parallel for shared(animals, addedAnimals, lock) firstprivate(newAnimal, rabbits) private(moveDir, hasRabbits, isFox, deadFoxes)
    for (i = 0; i < *animalCount; i++)
    {
        if (animals[i]->state == 'F')
        {
            hasRabbits = CheckRabbits(animals[i], rabbits, *countRabbits);

            if (hasRabbits > 0)
                animals[i]->starve = 0;
            else if (animals[i]->state == 'F')
            {
                animals[i]->starve++;
            }
            if (animals[i]->starve > 0 && animals[i]->starve == foodGen)
            {
                animals[i]->state = 'D';
            }
        }
        if (animals[i]->age > 0 && animals[i]->age == procGen && animals[i]->state != 'D')
        {
            omp_set_lock(&lock);
            if (animals[i]->age > 0 && animals[i]->age == procGen && animals[i]->state != 'D')
            {
                animals[i]->age = 0;
                *newAnimal = *animals[i];
                newAnimal->directions = 15;
                SetBoundaries(&newAnimal->directions, newAnimal->posX, newAnimal->posY, row, col);
                newAnimal->starve = 0;
                addedAnimals[addedCells++] = *newAnimal;
            }
            omp_unset_lock(&lock);
        }
        else if (animals[i]->age >= 0)
        {
            animals[i]->age++;
        }
        if (animals[i]->state != 'D')
        {
            moveDir = 0;
            //printf("this is a %c with posX -> %d and posY %d\n", animals[i]->state, animals[i]->posX, animals[i]->posY);
            moveDir = CriteriaAdjCell(currentGen, animals[i]);
            //printf("And has chosen %d\n", moveDir);
            ChangePos(animals[i], moveDir);
            animals[i]->directions = 15;
            SetBoundaries(&animals[i]->directions, animals[i]->posX, animals[i]->posY, row, col);
        }
    }
#pragma omp barrier
    if (addedCells > 0)
    {
        *animalCount = *animalCount + addedCells;
        *newcell = addedCells;
    }
    omp_destroy_lock(&lock);
    //free(&newAnimal);
    return addedAnimals;
}
void UpdateAnimals(cell *cells, int n, cell *rabbits[], int *rabbitsCount, cell *foxes[], int *foxesCount)
{
    int r = 0, f = 0, j = 0;
    omp_lock_t lock;
    omp_init_lock(&lock);
#pragma omp parallel shared(rabbits, foxes, cells, lock)
    {
#pragma omp for
        for (j = 0; j < n; j++)
        {
            if (cells[j].state == 'R')
            {
                omp_set_lock(&lock);
                if (cells[j].state == 'R')
                {
                    rabbits[r] = &cells[j];
                    r++;
                }
                omp_unset_lock(&lock);
            }
            else if (cells[j].state == 'F')
            {
                omp_set_lock(&lock);
                if (cells[j].state == 'F')
                {
                    foxes[f] = &cells[j];
                    f++;
                }
                omp_unset_lock(&lock);
            }
        }
        *rabbitsCount = r;
        *foxesCount = f;
    }
    omp_destroy_lock(&lock);
}

void SortAnimals(cell *animals, int *animalCount, int col)
{
    int i, key = 0, j = 0;
    cell *aux = (cell *)malloc(sizeof(cell));

    // omp_lock_t lock;
    // omp_init_lock(&lock);

//#pragma omp parallel for firstprivate(aux, key) shared(animals, lock)
    for (i = 1; i < *animalCount; i++)
    {
        *aux = animals[i];
        if (((animals[i].posX * col) + animals[i].posY) < (col * col))
            key = ((animals[i].posX * col) + animals[i].posY);
        //omp_set_lock(&lock);
        j = i - 1;
        //omp_unset_lock(&lock);

        while (j >= 0 && ((animals[j].posX * col) + animals[j].posY) > key)
        {
           // omp_set_lock(&lock);
            if (j >= 0 && ((animals[j].posX * col) + animals[j].posY) > key)
            {
                animals[j + 1] = animals[j];
                j = j - 1;
            }
           // omp_unset_lock(&lock);
        }        
        animals[j + 1] = *aux;
    }
    //omp_destroy_lock(&lock);
}

int removeDuplicates(cell *animals, int *animalCount, int col, int *countRabbits, int *countFoxes, int const maxSize)
{
    SortAnimals(animals, animalCount, col);

    if (*animalCount == 0 || *animalCount == 1)
        return 0;

    int j = 0, i = 0, k = 0;
    cell *temp[maxSize];
    // omp_lock_t lock;
    // omp_init_lock(&lock);

    //#pragma omp parallel shared(animals, countRabbits, countFoxes, lock)
    //    {
    //#pragma omp for
    for (i = 0; i < *animalCount - 1; i++)
    {
        if (animals[i].state != 'D')
        {
            if (animals[i].state != '*' && animals[i].posX == animals[i + 1].posX && animals[i].posY == animals[i + 1].posY)
            {
                if (animals[i].state == animals[i + 1].state)
                {
                    if (animals[i].age <= animals[i + 1].age)
                    {
                        if (animals[i].age == animals[i + 1].age && animals[i].starve >= animals[i + 1].starve)
                        {
                            if (animals[i].state == 'R')
                            {
                                //omp_set_lock(&lock);
                                if (animals[i].state == 'R')
                                {

                                    *countRabbits = *countRabbits - 1;
                                }
                                //omp_unset_lock(&lock);
                            }
                            else if (animals[i].state == 'F')
                            {
                                //omp_set_lock(&lock);
                                if (animals[i].state == 'F')
                                {
                                    *countFoxes = *countFoxes - 1;
                                }
                                //omp_unset_lock(&lock);
                            }
                        }
                    }
                }
                else
                {
                    if (animals[i].state == 'F' && animals[i + 1].state == 'R')
                    {
                        //omp_set_lock(&lock);
                        if (animals[i].state == 'F' && animals[i + 1].state == 'R')
                        {
                            animals[i + 1] = animals[i];
                            *countRabbits = *countRabbits - 1;
                        }
                        //omp_unset_lock(&lock);
                    }
                    else if (animals[i].state == 'R' && animals[i + 1].state == 'F')
                    {
                        //omp_set_lock(&lock);
                        if (animals[i].state == 'R' && animals[i + 1].state == 'F')
                        {
                            *countRabbits = *countRabbits - 1;
                        }
                        //omp_unset_lock(&lock);
                    }
                }
            }
            else
            {
                // omp_set_lock(&lock);
                // if (animals[i].state == '*'
                // ||( animals[i].posX != animals[i + 1].posX && animals[i].posY == animals[i + 1].posY)
                // ||( animals[i].posX == animals[i + 1].posX && animals[i].posY != animals[i + 1].posY)
                // ||( animals[i].posX != animals[i + 1].posX && animals[i].posY != animals[i + 1].posY))
                // {
                //printf("%d\n", j);
                //#pragma omp single private(j)
                temp[j++] = &animals[i];
                // }
                // omp_unset_lock(&lock);
            }
        }
        else if (animals[i].state == 'D')
        {
            //               omp_set_lock(&lock);
            if (animals[i].state == 'D')
            {
                *countFoxes = *countFoxes - 1;
            }
            //               omp_unset_lock(&lock);
        }
    }
    //}
    //    omp_destroy_lock(&lock);
    //#pragma omp barrier

    if (animals[*animalCount - 1].state != 'D')
        temp[j++] = &animals[*animalCount - 1];

    if (j < *animalCount)
    {
        // #pragma omp for
        for (k = 0; k < j; k++)
        {
            //printf("posX -> %d posY ->%d\n", temp[k]->posX, temp[k]->posY);
            animals[k] = *temp[k];
        }
    }
    // #pragma omp barrier
    *animalCount = j;

    return 1;
}

int CheckRabbits(cell *foxPos, cell *rabbits[], int countRabbits)
{
    char aux = (char)0;
#pragma omp parallel for schedule(dynamic) firstprivate(rabbits, foxPos)
    for (int i = 0; i < countRabbits; i++)
    {
        if (foxPos->posY == rabbits[i]->posY)
        {
            if ((foxPos->posX + 1) == rabbits[i]->posX && CheckDirection(&aux, 3) == 0)
            {
                SetDirection(&aux, 3);
            }
            if ((foxPos->posX - 1) == rabbits[i]->posX && CheckDirection(&aux, 1) == 0)
            {
                SetDirection(&aux, 1);
            }
        }
        else if (foxPos->posX == rabbits[i]->posX)
        {
            if ((foxPos->posY + 1) == rabbits[i]->posY && CheckDirection(&aux, 2) == 0)
            {
                SetDirection(&aux, 2);
            }
            if ((foxPos->posY - 1) == rabbits[i]->posY && CheckDirection(&aux, 4) == 0)
            {
                SetDirection(&aux, 4);
            }
        }
    }
    if (aux == (char)0)
        return 0;
    else
    {
        foxPos->directions = aux;
        return 1;
    }
}

int CriteriaAdjCell(int currentGen, cell *cell)
{
    int numberDir = 0;
    int possibleDir[] = {0, 0, 0, 0};
    int newDir = 0;

    for (int i = 1; i <= 4; i++)
    {

        if (CheckDirection(&cell->directions, i) == 1)
        {
            possibleDir[numberDir] = i;
            numberDir++;
        }
    }
    if (numberDir > 0)
    {
        newDir = possibleDir[((currentGen + cell->posX + cell->posY) % numberDir)];
    }
    // for (int i = 0; i < 4; i++)
    // {
    //     printf("this animal has %d direction\n", possibleDir[i]);
    // }
    return newDir;
}

void UpdateAdjCells(cell *cells, int n)
{
    cell *aux;
    char *directionAux;
    int j, i;
#pragma omp parallel for schedule(static) lastprivate(aux, directionAux) shared(cells)
    for (i = 0; i < n; i++)
    {
        aux = &cells[i];
        directionAux = &aux->directions;

        if (aux->state != '*' && aux->state != 'D')
        {
#pragma omp parallel for schedule(static) firstprivate(aux, directionAux) shared(cells)
            for (j = 0; j < n; j++)
            {
                if (aux->state == 'R')
                {
                    if (cells[j].posY == aux->posY)
                    {
                        if (cells[j].posX == (aux->posX + 1) && CheckDirection(&aux->directions, 3) == 1)
                        {
                            *directionAux = SetDirection(&aux->directions, 3);
                        }
                        else if (cells[j].posX == (aux->posX - 1) && CheckDirection(&aux->directions, 1) == 1)
                        {
                            *directionAux = SetDirection(&aux->directions, 1);
                        }
                    }
                    else if (cells[j].posX == aux->posX)
                    {
                        if (cells[j].posY == (aux->posY + 1) && CheckDirection(&aux->directions, 2) == 1)
                        {
                            *directionAux = SetDirection(&aux->directions, 2);
                        }
                        else if (cells[j].posY == (aux->posY - 1) && CheckDirection(&aux->directions, 4) == 1)
                        {
                            *directionAux = SetDirection(&aux->directions, 4);
                        }
                    }
                }
                else if (aux->state == 'F')
                {
                    if (cells[j].posY == aux->posY)
                    {
                        if (cells[j].posX == (aux->posX + 1) && CheckDirection(&aux->directions, 3) == 1 && (cells[j].state != 'R'))
                        {
                            *directionAux = SetDirection(&aux->directions, 3);
                        }
                        else if (cells[j].posX == (aux->posX - 1) && CheckDirection(&aux->directions, 1) == 1 && (cells[j].state != 'R'))
                        {
                            *directionAux = SetDirection(&aux->directions, 1);
                        }
                    }
                    else if (cells[j].posX == aux->posX)
                    {
                        if (cells[j].posY == (aux->posY + 1) && CheckDirection(&aux->directions, 2) == 1 && (cells[j].state == 'F'))
                        {
                            *directionAux = SetDirection(&aux->directions, 2);
                        }
                        else if (cells[j].posY == (aux->posY - 1) && CheckDirection(&aux->directions, 4) == 1 && (cells[j].state == 'F'))
                        {
                            *directionAux = SetDirection(&aux->directions, 4);
                        }
                    }
                }
            }
            // printf("%d", omp_get_num_threads());
        }
    }
}

cell *BoardFix(cell *cells, int rows, int colum, int n)
{

    cell *finalBoard = (cell *)malloc(rows * colum * (sizeof(cell)));
    assert(finalBoard != NULL);
    cell aux = {0, 0, '.'};
    int i, j, k;
#pragma omp parallel for schedule(static) private(i) shared(finalBoard)
    for (i = 0; i < colum; i++)
    {
#pragma omp parallel for schedule(static) private(j) shared(finalBoard)

        for (j = 0; j < rows; j++)
        {
            finalBoard[(i * colum + j)] = aux;
        }
    }
#pragma omp parallel for schedule(static) private(i) shared(finalBoard)
    for (i = 0; i < colum; i++)
    {
#pragma omp parallel for schedule(static) private(j) shared(finalBoard)
        for (j = 0; j < rows; j++)
        {
#pragma omp parallel for schedule(static) private(k) shared(finalBoard) firstprivate(cells)
            for (k = 0; k < n; k++)
            {
                if (cells[k].posX == i && cells[k].posY == j)
                {
                    finalBoard[(i * colum + j)].state = cells[k].state;
                }
            }
        }
    }
    return finalBoard;
}

void BoardPrint(cell *cells, int rows, int colum)
{
    int i, j;
    //#pragma omp parallel private(j) shared(cells)
    for (i = 0; i < colum; i++)
    {
        //#pragma omp parallel private(j) shared(cells)
        for (j = 0; j < rows; j++)
        {
            //#pragma omp single nowait
            printf(" %c ", cells[(i * colum + j)].state);
        }
        //#pragma omp single nowait
        printf("\n");
    }
    fflush(stdout);
}

void ClearScreen()
{
#ifdef WINDOWS
    system("CLS");
#else
    int some = system("clear");
#endif
}
#pragma endregion
