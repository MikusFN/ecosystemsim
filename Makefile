CC = g++
CCFLAGS = -ffast-math -Ofast -lm

OUT = EcoSIM
SRC = Source/EcoSystemSimMainUbuntu.c
HOSTFILE = host.txt

default: build

build:
	${CC} ${CCFLAGS} ${SRC} -o ${OUT} -fopenmp

test: build
	./${OUT} < Inputs/input5x5


